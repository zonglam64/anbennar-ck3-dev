@smErmine = 0.27


dynasty_silmuna = { # Silmuna
	pattern = "pattern_solid.dds"	
	color1 = "damerian_black"	
	color2 = "damerian_black"	
	colored_emblem = {
		texture = "ce_anb_elven_ship.dds"
		color1 = "damerian_white"
		color2 = "damerian_blue"
		instance = { position = { 0.5 0.5 }	scale = { 0.8 0.8 }	}
	}	
}

dynasty_siloriel = { # Siloriel
	pattern = "pattern_solid.dds"	
	color1 = "damerian_white"	
	color2 = "damerian_white"	
	colored_emblem = {
		texture = "ce_anb_elvenized_rose.dds"
		color1 = "lorentish_red"
		color2 = "lorentish_red_dark"
	}	
}

9 = { # Pearlman
	pattern = "pattern_solid.dds"
	color1 = "pearlsedge_blue"	
	color2 = "pearlsedge_blue"	
	colored_emblem = {
		texture = "ce_circle.dds"
		color1 = "pearlsedge_pearl"
		instance = { position = { 0.5 0.4 }	scale = { 0.8 0.8 }	}
	}
}

12 = { # Deranne
	pattern = "pattern_vertical_split_01.dds"
	color1 = "pearlsedge_blue"	
	color2 = "lorentish_green"
	colored_emblem = {
		texture = "ce_anb_lorentish_circlet.dds"
		color1 = "damerian_white"
		color2 = "damerian_white"
		instance = { position = { 0.5 0.5 }	scale = { 0.9 0.9 }	}
	}
	colored_emblem = {
		texture = "ce_anb_elvenized_rose.dds"
		color1 = "derannic_purple"
		color2 = "derannic_pink"
		instance = { position = { 0.5 0.5 }	scale = { 0.7 0.7 }	}
	}
}

14 = { # Lorentis
	pattern = "pattern_solid.dds"	
	color1 = 	"lorentish_lilac"
	color2 = 	"lorentish_lilac"
	colored_emblem = {
		texture = "ce_flower.dds"
		color1 = "lorentish_red_dark"
		color2 = "lorentish_green"
	}	
}

house_dameris = { # Dameris
	pattern = "pattern_solid.dds"	
	color1 = "damerian_blue"	
	color2 = "damerian_blue"
	colored_emblem = {
		texture = "ce_anb_old_damerian_moon.dds"
		color1 = "damerian_white"
		color2 = "damerian_white"
		instance = { position = { 0.5 0.5 }	scale = { 0.6 0.6 }	}
	}
}

house_lorentis = { # Lorentis
	pattern = "pattern_solid.dds"	
	color1 = 	"lorentish_lilac"
	color2 = 	"lorentish_lilac"
	colored_emblem = {
		texture = "ce_flower.dds"
		color1 = "lorentish_red_dark"
		color2 = "lorentish_green"
	}	
}

house_rewantis = { # Rewantis
	pattern = "pattern_solid.dds"
	color1 = "lorentish_red_dark"
	color2 = "lorentish_red_dark"
	colored_emblem = {
		texture = "ce_anb_redglades.dds"
		color1 = "white"
		color2 = "white"
		instance = { position = { 0.5 0.5 }	scale = { 0.8 0.8 }	}
	}
}

11 = { # sil Vis
	pattern = "pattern_solid.dds"
	color1 = "damerian_white"	
	color2 = "damerian_white"
	colored_emblem = {
		texture = "ce_anb_ordinary_pale_5.dds"
		color1 = rgb { 183 23 31 } #red
		color2 = rgb { 183 23 31 } #red
		instance = { position = { 0.08 0.5 }	scale = { 0.71 1.0 }	}
	}
	colored_emblem = {
		texture = "ce_anb_ordinary_pale_5.dds"
		color1 = rgb { 214 86 32 } #orange
		color2 = rgb { 214 86 32 } #orange
		instance = { position = { 0.22 0.5 }	scale = { 0.71 1.0 }	}
	}
	colored_emblem = {
		texture = "ce_anb_ordinary_pale_5.dds"
		color1 = rgb { 216 191 23 } #yellow
		color2 = rgb { 216 191 23 } #yellow
		instance = { position = { 0.36 0.5 }	scale = { 0.71 1.0 }	}
	}
	colored_emblem = {
		texture = "ce_anb_ordinary_pale_5.dds"
		color1 = rgb { 10 102 49 } #green
		color2 = rgb { 10 102 49 } #green
		instance = { position = { 0.5 0.5 }	scale = { 0.71 1.0 }	}
	}
	colored_emblem = {
		texture = "ce_anb_ordinary_pale_5.dds"
		color1 = "blue"
		color2 = "blue"
		instance = { position = { 0.64 0.5 }	scale = { 0.71 1.0 }	}
	}
	colored_emblem = {
		texture = "ce_anb_ordinary_pale_5.dds"
		color1 = rgb { 77 42 124 } #dark purple
		color2 = rgb { 77 42 124 } #dark purple
		instance = { position = { 0.78 0.5 }	scale = { 0.71 1.0 }	}
	}
	colored_emblem = {
		texture = "ce_anb_ordinary_pale_5.dds"
		color1 = rgb { 143 85 163 } #light purple
		color2 = rgb { 143 85 163 } #light purple
		instance = { position = { 0.92 0.5 }	scale = { 0.71 1.0 }	}
	}
}

dynasty_roilsardis = {
	pattern = "pattern_solid.dds"
	color1 = "roilsardi_red"	
	color2 = "roilsardi_red"
	colored_emblem = {
		texture = "ce_anb_roilsardi_thorn.dds"
		color1 = "roilsardi_green"
		color2 = "roilsardi_green"
		instance = { position = { 0.5 0.5 }	scale = { 0.9 0.9 }	}
	}	
}

house_roilsard = {
	pattern = "pattern_solid.dds"
	color1 = "roilsardi_red"	
	color2 = "roilsardi_red"
	colored_emblem = {
		texture = "ce_anb_roilsardi_thorn.dds"
		color1 = "roilsardi_green"
		color2 = "roilsardi_green"
		instance = { position = { 0.5 0.5 }	scale = { 0.9 0.9 }	}
	}	
}

house_loop = {
	pattern = "pattern_solid.dds"
	color1 = "roilsardi_green"	
	color2 = "roilsardi_green"
	colored_emblem = {
		texture = "ce_anb_roilsardi_thorn.dds"
		color1 = "blue"
		color2 = "blue"
		instance = { position = { 0.5 0.5 }	scale = { 0.9 0.9 }	}
	}	
}


house_saloren = {
	pattern = "pattern_solid.dds"
	color1 = rgb { 244 132 114 }
	color2 = rgb { 244 132 114 }
	colored_emblem = {
		texture = "ce_anb_roilsardi_thorn.dds"
		color1 = "roilsardi_green"
		color2 = "roilsardi_green"
		instance = { position = { 0.5 0.5 }	scale = { 0.9 0.9 }	}
	}	
}


house_vivin = {
	pattern = "pattern_solid.dds"
	color1 = "corvurian_red_light"	
	color2 = "corvurian_red_light"
	colored_emblem = {
		texture = "ce_anb_roilsardi_thorn.dds"
		color1 = "black"
		color2 = "black"
		instance = { position = { 0.5 0.5 }	scale = { 0.9 0.9 }	}
	}	
}

dynasty_gawe = {
	pattern = "pattern_solid.dds"
	color1 = "gawedi_blue"
	color2 = "gawedi_blue"
	colored_emblem = {
		texture = "ce_eagle.dds"
		color1 = rgb { 209 173 103 }
		color2 = "corvurian_red_light"
		instance = { position = { 0.5 0.5 } scale = { 1.0 1.0 }  }
	}
}

house_coldsteel = {
	pattern = "pattern_solid.dds"
	color1 = "grey"
	color2 = "grey"
	colored_emblem = {
		texture = "ce_eagle.dds"
		color1 = rgb { 209 173 103 }
		color2 = "corvurian_red_light"
		instance = { position = { 0.5 0.5 } scale = { 1.0 1.0 }  }
	}
}


dynasty_silebor = {
	pattern = "pattern_solid.dds"
	color1 = "damerian_black"	
	color2 = "damerian_black"
	colored_emblem = {
		texture = "ce_waves_04.dds"
		color1 = rgb { 237 223 131 }
		color2 = rgb { 237 223 131 }
		instance = { position = { 0.5 0.76 } scale = { 1.5 1.5 }  }
		instance = { position = { 0.5 1.11 } scale = { 1.5 1.5 }  }
	}
	colored_emblem = {
		texture = "ce_waves_04.dds"
		color1 = rgb { 230 179 54 }
		color2 = rgb { 230 179 54 }
		instance = { position = { 0.5 0.85 } scale = { 1.5 1.5 }  }
	}
	colored_emblem = {
		texture = "ce_anb_dragon_rampant.dds"
		color1 = rgb { 79 139 159 }
		color2 = "white"
		instance = { position = { 0.5 0.45 } scale = { 0.7 0.7 }  }
	}
}

dynasty_silistra = {
	pattern = "pattern_solid.dds"
	color1 = "purpure"
	color2 = "purpure"
	colored_emblem = {
		texture = "ce_anb_sea_lion_rampant.dds"
		color1 = "yellow_light"
		color2 = rgb { 34 165 178 }	#turquoise
		instance = { scale = { -0.8 0.8 }  }
	}	
}