﻿fallback_army_01 = {
	type = army
	entity = unit_western_infantry_01_entity
}


# Common Dwarf

common_dwarf_army_01 = {
	type = army
	graphical_cultures = { german_group_coa_gfx }
	entity = unit_common_dwarf_infantry_01_entity
}

common_dwarf_army_03 = {
	type = army
	quality = 3
	graphical_cultures = { german_group_coa_gfx }
	entity = unit_common_dwarf_infantry_02_entity
}

common_dwarf_army_05 = {
	type = army
	quality = 5
	graphical_cultures = { german_group_coa_gfx }
	entity = unit_common_dwarf_infantry_03_entity
}


